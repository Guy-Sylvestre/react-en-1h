import { useState } from "react";
import { Tweet } from "./app/Tweet";


const DEFAULT_TWEET = [
    {
        id: 1,
        like: 2,
        content: "Je vais bien",
        name: "Guy"
    }, {
        id: 2,
        like: 20,
        content: "Je vais tres bien",
        name: "Sylvestre"
    }, {
        id: 3,
        like: 233,
        content: "Je vais au cours",
        name: "KRAKOU"
    }, {
        id: 4,
        like: 100,
        content: "Je vais au marche",
        name: "Emmanuel"
    }, {
        id: 5,
        like: 90,
        content: "Je vais au champ",
        name: "JEan"
    }, {
        id: 6,
        like: 30,
        content: "Je vais chez mon boss",
        name: "Jean-Eud"
    }, {
        id: 7,
        like: 6,
        content: "Je vais coder",
        name: "Chris"
    }, {
        id: 8,
        like: 90,
        content: "J'aprends les base de reactjs'",
        name: "Atcho"
    }, {
        id: 9,
        like: 7,
        content: "La decouvert de css grid est magnifique",
        name: "Mois"
    }, {
        id: 10,
        like: 11,
        content: "Je doit dresser la list du type de femme que je souhait avoir`",
        name: "Enock"
    }, {
        id: 11,
        like: 10,
        content: "Je sens que je deviens encpre plus fort",
        name: "Enderson"
    }, {
        id: 12,
        like: 0,
        content: "C'est quoi pour toi le sens du sacrifice",
        name: "Michael"
    }, {
        id: 13,
        like: 4,
        content: "Comment devenue meilleur en programmqtion python et javascript",
        name: "Eudes"
    }, {
        id: 14,
        like: 2,
        content: "Je vais chez mon ami",
        name: "Cedric"
    },
]

const App = () => {

    let [tweet, setTweet] = useState(DEFAULT_TWEET);

    const handleDelete = (id) => {
        setTweet((curr) => curr.filter((tweet) => tweet.id !== id))
    }
    return (
        <>
            <div className="content-tweet">
                {
                    tweet.map((item) => {
                        return (
                            <Tweet
                                key={item.id}
                                like={item.like}
                                id={item.id}
                                content={item.content}
                                name={item.name}
                                onDelete={(id) => handleDelete(id)}
                            />
                        )
                    })
                }
            </div>
        </>
    );
};

export default App;