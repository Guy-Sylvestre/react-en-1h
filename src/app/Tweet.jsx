/* eslint-disable react/prop-types */


export const Tweet = ({ id, name, content, like, onDelete }) => {

    const handleLike = () => {
        console.log("LIKED", name);
    }
    return (
        <>
            <div className="tweet">
                <svg onClick={() => onDelete(id)} xmlns="http://www.w3.org/2000/svg" className="icon-svg close-card icon-svg-pointer" viewBox="0 0 24 24"><path fill="currentColor" d="M19 6.41L17.59 5L12 10.59L6.41 5L5 6.41L10.59 12L5 17.59L6.41 19L12 13.41L17.59 19L19 17.59L13.41 12z"></path></svg>
                <div className="header">
                    <h1 className="title">{name}</h1>
                </div>
                <div className="body">
                    <p>{content}</p>
                </div>
                <div className="footer">
                    <button onClick={() => handleLike()} className="btn-custom">
                        <span className="like-number"> {like} </span>
                        <svg xmlns="http://www.w3.org/2000/svg" className="icon-svg icon-svg-pointer" viewBox="0 0 24 24"><path fill="currentColor" d="M13.35 20.13c-.76.69-1.93.69-2.69-.01l-.11-.1C5.3 15.27 1.87 12.16 2 8.28c.06-1.7.93-3.33 2.34-4.29c2.64-1.8 5.9-.96 7.66 1.1c1.76-2.06 5.02-2.91 7.66-1.1c1.41.96 2.28 2.59 2.34 4.29c.14 3.88-3.3 6.99-8.55 11.76z"></path></svg>
                    </button>
                </div>
            </div>
        </>
    );
};
